# universal

Universal Workflow Content Pack and Information

# Design

Pipeline is a standardized sequence of Workflows that are automatically chained together by Digital Rebar. Behavior of a Pipeline is determined by Parameters assigned to the machine by a Pipeline Profile. These parameters can determine the sequence of workflows in the pipeline chain and additional behaviors such as classification, callbacks and task injection (flexiflow).

# Meta Data Conventions

## Common

color: olive
icon: map signs (generall), code branch (chaining)
title: RackN Content

## Workflows

pipeline: start

## Stages

workflow: [universal-workflow]

# Stages in Pipeline Workflows

These sections help define the common components of a workflow.

## Common Stages:

- discover for actual machines
- start for containers, clusters and resource brokers

## PRE

- universal-[workflow]-start-callback
- universal-[workflow]-pre-flexiflow

## PAYLOAD

This is the workflow specific items

- CUSTOM STAGES PART 1
- MID
- CUSTOM STAGES PART 2

## POST

- universal-[workflow]-post-flexiflow
- universal-[workflow]-classification
- universal-[workflow]-post-validation
- universal-[workflow]-complete-callback

## REQUIRED EXIT

- universal-chain-workflow

.. Release v4.9.0 Start

.. Release v4.10.0 Start

.. Release v4.11.0 Start

.. Release v4.12.0 Start

.. Release v4.13.0 Start

.. Release v4.14.0 Start

.. Release v4.15.0 Start
