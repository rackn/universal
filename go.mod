module gitlab.com/rackn/universal/v4

go 1.16

require (
	github.com/BurntSushi/toml v1.4.0 // indirect
	github.com/itchyny/gojq v0.12.13
	github.com/stevenroose/remarshal v0.0.0-20160825110430-37e7cbf22025 // indirect
	gitlab.com/rackn/provision/v4 v4.14.0
)
