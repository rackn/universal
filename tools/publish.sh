#!/usr/bin/env bash

set -e

[[ $GOPATH ]] || export GOPATH="$HOME/go"
fgrep -q "$GOPATH/bin" <<< "$PATH" || export PATH="$PATH:$GOPATH/bin"

unset GO111MODULE
go get -u github.com/stevenroose/remarshal
go install github.com/stevenroose/remarshal

version=$(tools/version.sh)

. tools/array.sh

for i in "${arr[@]}" ; do
    echo "Publishing $i to cloud"
    CONTENT=$i
    mkdir -p "rebar-catalog/$CONTENT"
    remarshal -i $CONTENT.yaml -o $CONTENT.json -if yaml -of json
    cp $CONTENT.json rebar-catalog/$CONTENT/$version.json
done

