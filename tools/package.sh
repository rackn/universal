#!/usr/bin/env bash

set -e

case $(uname -s) in
    Darwin) shasum="command shasum -a 256";;
    Linux) shasum="command sha256sum";;
    *)
        # Someday, support installing on Windows.  Service creation could be tricky.
        echo "No idea how to check sha256sums"
        exit 1;;
esac

export GO111MODULE=on

mkdir -p tools/build
exepath="$PWD/tools/build"
[[ -x $exepath/drpcli ]] || \
    go build -o "$exepath/drpcli" tools/drpcli.go
export PATH="$PWD/tools/build:$PATH"

version=$(tools/version.sh)
. tools/version.sh
branch=${MajorV}.${MinorV}

DOC_VERSION=""
if [[ "$CI_COMMIT_BRANCH" != "" ]] ; then
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%%-*}
  CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%.*}
  DOC_VERSION=$CI_COMMIT_BRANCH
fi
if [[ "$CI_COMMIT_TAG" != "" ]] ; then
  CI_COMMIT_TAG=${CI_COMMIT_TAG%%-*}
  CI_COMMIT_TAG=${CI_COMMIT_TAG%.*}
  DOC_VERSION=$CI_COMMIT_TAG
fi
if [[ "$DOC_VERSION" == "v4" ]] ; then
  DOC_VERSION=tip
fi
if [[ "$VERSION" == "" ]] ; then
  DOC_VERSION=tip
fi

mkdir -p rebar-catalog/docs/$branch
. tools/array.sh
for i in "${arr[@]}"; do
    cd $i
    rm -f ._Version.meta
    drpcli contents bundle ../$i.yaml Version=$version
    cd ..
    $shasum $i.yaml > $i.sha256
    echo "Publishing docs"
    drpcli contents document $i.yaml > rebar-catalog/docs/$branch/$i.rst
    echo "Building documents $DOC_VERSION: $i"
    drpcli contents document-md $i.yaml rackn-base-docs/$DOC_VERSION || :
done

