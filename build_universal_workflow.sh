#!/usr/bin/env bash
# Script to create a universal workflow

echo "DO NOT USE THIS SCRIPT ANYMORE!  There have been changes!"
exit 1

###
#  This tool creates a Digital Rebar Provision universal workflow.

#  USAGE: run with the '-u' flag to see  full usage instructions
###

export PS4='${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): '
set -e

# global variables used through this script
declare -i dbg=0                 # debug output off by default
declare OUT_DIR="content"        # where output data will be written to
declare CONTENT=""               # create full content pack from ISOs
declare DO_BUNDLE=""             # attempt to do a 'drpcli contents bundle ...'
declare DO_UPLOAD=""             # attempt to do a 'drpcli contents upload ...'
declare -a COMPONENTS=()         # components
declare -A WORKFLOW_MAP          # A map of stages for the basic workflows.

WORKFLOW_MAP=(
  [baseline]="discover,callback:start,flexiflow:pre,centos-setup-repos,ipmi-inventory,raid-inventory,network-lldp,inventory,bios-baseline,raid-baseline,validation-record-parameters,universal-baseline-profiles,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [discover]="discover,callback:start,flexiflow:pre,centos-setup-repos,ipmi-inventory,raid-inventory,bios-inventory,network-lldp,inventory,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [hardware]="discover,callback:start,flexiflow:pre,centos-setup-repos,hardware-tools-install,ipmi-configure,flash,raid-enable-encryption,raid-configure,bios-configure,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [burnin]="discover,callback:start,flexiflow:pre,centos-setup-repos,burnin,burnin-reboot,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [decommission]="discover,callback:start,flexiflow:pre,universal-decommission,raid-reset,raid-enable-encryption,shred,raid-reset,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [local]="callback:start,flexiflow:pre,ssh-access,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
  [maintenance]="discover,callback:start,flexiflow:pre,universal-enter-maintenance,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [rebuild]="discover,callback:start,flexiflow:pre,raid-reset,vmware-esxi-clear-patch-index,universal-rebuild,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [solidfire]="discover,callback:start,flexiflow:pre,universal-solidfire-install,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
  [image-deploy]="discover,callback:start,flexiflow:pre,image-deploy,image-deploy-cloud-init,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
  [bootstrap]="callback:start,flexiflow:pre,universal-bootstrap,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete-nobootenv"
  [linux-install]="discover,callback:start,flexiflow:pre,prep-install,linux-selector,configure-network,ssh-access,drp-agent,flexiflow:during-install,finish-install,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
  # Left for those who might use it.
  [esxi-install]="discover,callback:start,flexiflow:pre,prep-install,vmware-esxi-clear-patch-index,vmware-esxi-set-password,vmware-esxi-selector,esxi-preserve-logs,finish-install,esxi-acceptance-level,esxi-remove-vmware-bootorder,esxi-rename-datastore,esxi-preserve-logs,esxi-install-patches,esxi-activate-network,esxi-activate-shells,esxi-activate-nested,esxi-activate-password-policy,esxi-install-welcome,esxi-install-certificate,esxi-preserve-logs,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
  # Kickstart/ISO-based esxi install
  [esxi-kickstart]="discover,callback:start,flexiflow:pre,prep-install,vmware-esxi-clear-patch-index,vmware-esxi-set-password,vmware-esxi-selector,esxi-preserve-logs,flexiflow:during-install,finish-install,esxi-acceptance-level,esxi-remove-vmware-bootorder,esxi-rename-datastore,esxi-preserve-logs,esxi-install-patches,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
  # ESXI config pieces to be used by both esxi-kickstart and image-deploy - use application-based workflow-chain-map to hook them together
  [esxi-config]="callback:start,flexiflow:pre,esxi-acceptance-level,esxi-rename-datastore,esxi-activate-network,esxi-activate-shells,esxi-activate-nested,esxi-activate-password-policy,esxi-install-welcome,esxi-install-certificate,esxi-preserve-logs,flexiflow:post,classify:,validation:post,callback:complete,universal-chain-workflow,complete"
)

###
#  Helper function to get and print the keys of the WORKFLOW_MAP
###
function print_map_keys() {
  # print the full map table
  printf "%s\n" "${!WORKFLOW_MAP[@]}"
}

###
#  Just print the WORKFLOW_MAP values for reference.
###
function print_map_info() {
  echo "WORKFLOW_MAP information is set as follows:"
  echo ""
  KEYS="$(print_map_keys)"
  for ISO_NAME in $KEYS; do
      echo "$ISO_NAME = ${WORKFLOW_MAP[$ISO_NAME]}"
  done
} # end print_map_info()

###
#  Output our usage statement
###
function usage() {
  _scr=$(basename $0)
  _pad=$(printf "%${#_scr}s" " ")
  cat <<END_USAGE
USAGE:  $_scr [ -AxBUdu ] [ -c name ] [ -o output_dir ] <workflow names ...>

        -o output_dir    Set the specified directory to output content to
        -c name          Create full Content Pack in the '-o output_dir' which
                         includes workflows, stages, etc.
        -A               All known items
        -B               attempt to do a 'drpcli contents bundle ...' operation
                         (requires '-c', and RS_ENDPOINT, RS_KEY or similar)
        -U               attempt to do a 'drpcli contents upload ...' operation
                         (requires '-c', and RS_ENDPOINT, RS_KEY or similar)
        -u               Output the short usage statement
        -d               Turn on output debugging
        -x               Extended Usage
        -p               Print map

END_USAGE

  [[ -z "$XTND" ]] && printf ">>> For eXtended help output with Notes and Examples, use '-x'. <<<\n\n"

  if [[ "$XTND" ]]
  then
    cat <<XTND_USAGE

NOTES:  Fill in as needed

EXAMPLES:

  * Creates a basic universal workflow called discover:

        $_scr discover

  * Generate a content pack name my_content with discover and hardware defined:

        $_scr -c my_content discover hardware

XTND_USAGE
  fi # end extended usage output statement

} # end usage()

# because, once again - thank you Mac OS X ...
# BASH portable replacement for "readlink -f"
function get_realpath() {
    [[ ! -r "$1" ]] && xiterr 1 "file/dir '$1' does not exist"
    [[ -d "$1" ]] && { cd $1; echo $PWD; return 0; }
    [[ -n "$no_symlinks" ]] && local pwdp='pwd -P' || local pwdp='pwd'
    echo "$( cd "$( echo "${1%/*}" )" 2>/dev/null; $pwdp )"/"${1##*/}"
    return 0
}

###
#  try and provide info to clean up if we exit ungracefully
###
function got_trap() {
  echo ""
  echo "*******************************************************************************"
  echo "TRAP DEBUG:   LASTNO = $1"
  echo "TRAP DEBUG:   LINENO = $2"
  echo "TRAP DEBUG:  COMMAND = $3"
  echo "*******************************************************************************"
  echo ""
  echo "May need to nuke OUT_DIR dir ('$OUT_DIR')"
  echo ""
  echo " REMOVE:  rm -rf $OUT_DIR.*"
  GOT_TRAP="yes"
} # end got_trap()

###
#  try and unmount iso if in isomount mode, output various exit statements
###
function cleanup() {
  [[ $GOT_TRAP ]] && exit 1
  [[ $OUT_DIR ]] && echo "DRP Contents created in:  $OUT_DIR"
} # end cleanup()

###
#  Process any command line flags and arguments
###
function process_options() {
  local _print_map=0
  local _all=0
  while getopts ":uxdABUpc:o:t:" opt
  do
    case "${opt}" in
      u)  usage; exit 0                       ;;
      x)  XTND="true"; usage; exit 0          ;;
      d)  dbg=1                               ;;
      c)  CONTENT="$OPTARG"; DO_BUNDLE="true" ;;
      o)  OUT_DIR="$OPTARG"                   ;;
      B)  DO_BUNDLE="true"                    ;;
      U)  DO_UPLOAD="true"                    ;;
      p)  _print_map=1                        ;;
      A)  _all=1                              ;;
      \?) echo
          echo "Option does not exist : $OPTARG"
          usage
          exit 1
      ;;
    esac
  done
  shift $(expr $OPTIND - 1 )

  # print map info and exit if that's what was requested
  (( $_print_map )) && { print_map_info; exit 0; }

  if (( $_all )) ; then
    KEYS="$(print_map_keys)"
    for ISO_NAME in $KEYS; do
      COMPONENTS+=($ISO_NAME)
    done
  else
    for ISO_NAME in "$@"; do
      COMPONENTS+=($ISO_NAME)
    done
  fi

  [[ "$OUT_DIR" ]] && mkdir -p "$OUT_DIR" || OUT_DIR="$(mktemp -d /tmp/esxi-bootenv-$$.XXXXXXXX)"
  OUT_DIR=$(get_realpath "$OUT_DIR")

  [[ -d ${OUT_DIR}/templates ]] || mkdir -p "${OUT_DIR}/templates" || xiterr 1 "template dir exists already ($OUT_DIR/templates)"
  [[ -d ${OUT_DIR}/workflows ]] || mkdir -p "${OUT_DIR}/workflows" || xiterr 1 "workflows dir exists already ($OUT_DIR/workflows)"
  [[ -d ${OUT_DIR}/stages ]] || mkdir -p "${OUT_DIR}/stages" || xiterr 1 "template dir exists already ($OUT_DIR/stages)"
  [[ -d ${OUT_DIR}/params ]] || mkdir -p "${OUT_DIR}/params" || xiterr 1 "template dir exists already ($OUT_DIR/params)"

  [[ "$DO_BUNDLE" ]] && WHAT="Bundle" || true
  [[ "$DO_UPLOAD" ]] && WHAT="$WHAT Upload" || true
  # validate our options around Content, Bundle, and Upload
  if [[ "$CONTENT" ]]
  then
    if [[ "$DO_BUNDLE" || "$DO_UPLOAD" ]]
    then
      echo "Requesting content operations for: $WHAT"
    else
      echo "Requesting content pack creation, but no Bundle or Upload operations."
    fi
  else
    if [[ "$DO_BUNDLE" || "$DO_UPLOAD" ]]
    then
      xiterr 1 "$WHAT operation(s) requested, but no content create specified."
    fi
  fi

  echo "Output directory initialized:  $OUT_DIR"
} # end process_options()

function build_workflow() {
  local _BASE=$1
  local _NAME=universal-$_BASE
  local _DESCRIPTION=$2


  local _W_YAML="${OUT_DIR}/workflows/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: $_NAME
Description: "$_DESCRIPTION"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Stages:
WF

  STAGES=${WORKFLOW_MAP[$_BASE]}

  IFS=',' read -ra ADDR <<< "${STAGES}"
  for i in "${ADDR[@]}"; do
    stage=$i
    if  [[ "$i" == *":"* ]] ; then
      IFS=":" read -a parts <<< "$i"

      case "${parts[0]}" in
        classify)
          cname=${parts[1]}
          if [[ "$cname" != "" ]] ; then
            cname="-$cname"
          fi
          stage="universal-$_BASE$cname-classification"
          build_classification_stage "$stage"
          ;;
        callback)
          cname="${parts[1]}"
          if [[ $cname == "" ]] ; then
            echo "$_BASE callback needs a name"
            exit 1
          fi
          stage="universal-$_BASE-$cname-callback"
          build_callback_stage "$stage" "universal-$_BASE-$cname"
          ;;
        validation)
          cname="${parts[1]}"
          if [[ $cname == "" ]] ; then
            echo "$_BASE validation needs a name"
            exit 1
          fi
          stage="universal-$_BASE-$cname-validation"
          build_validation_stage "$stage"
          ;;
        flexiflow)
          cname="${parts[1]}"
          if [[ $cname == "" ]] ; then
            echo "$_BASE flexiflow needs a name"
            exit 1
          fi
          stage="universal-$_BASE-$cname-flexiflow"
          build_flexiflow_stage "$stage"
          ;;
        *)
          echo "Unknown ${parts[0]} in $_BASE"
          exit 1
          ;;
      esac
    fi

    echo "  - $stage" >> $_W_YAML
  done
}

function build_callback_stage() {
  local _NAME=$1
  local _ACTION=$2
  local _W_YAML="${OUT_DIR}/stages/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: $_NAME
Description: "callback stage $_NAME for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Params:
  callback/action: "$_ACTION"
  callback/event: true
Tasks:
  - callback-task
WF
}

function build_classification_stage() {
  local _NAME=$1
  local _pname=${_NAME/-/\/}
  local _W_YAML="${OUT_DIR}/stages/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: $_NAME
Description: "classification stage $_NAME for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Params:
  classify/stage-list-parameter: "${_pname}-list"
Tasks:
  - classify-stage-list-start
  - classify-stage-list-stop
WF

   clstage="$_NAME-base"

  _W_YAML="${OUT_DIR}/params/$_NAME-list.yaml"
  cat <<WF > $_W_YAML
---
Name: ${_pname}-list
Description: "classification param ${_pname}-list for universal workflow"
Documentation: "Provides a list of stages to run during this workflow"
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Schema:
  default:
    - $clstage
  type: "array"
  items:
    type: "string"
WF

   build_classifier_base_stage "$clstage"
}

function build_classifier_base_stage() {
  local _NAME="$1"
  local _pname=${_NAME/-/\/}

  local _W_YAML="${OUT_DIR}/stages/$_NAME.yaml"

  cat <<WF > $_W_YAML
---
Name: $_NAME
Description: "base classification stage $_NAME for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Params:
  classify/version: 2
  classify/data-parameter: ${_pname}-data
  classify/function-parameter: ${_pname}-functions
Tasks:
- classify
WF


  _W_YAML="${OUT_DIR}/params/${_NAME}-data.yaml"
  cat <<WF > $_W_YAML
---
Name: ${_pname}-data
Description: "The data for the $_NAME classifer."
Documentation: |
  This classifer is the default classifier for the $_NAME workflow.
Meta:
  color: yellow
  icon: shuffle
  populate: "true"
Schema:
  type: "array"
  items:
    type: "object"
    required:
      - "test"
      - "actions"
    properties:
      test:
        type: "string"
      continue:
        type: boolean
      actions:
        type: "array"
        items:
          type: "string"
WF
if [[ "$_NAME" == "universal-discover-classification-base" ]] ; then
  cat <<WF >> $_W_YAML
  default:
    - test: always
      actions:
        - set_hardware_param
      continue: true
    - test: always
      actions:
        - convert_rack_build_to_universal_application
      continue: true
    - test: always
      actions:
        - cl_process_param_to_profile universal/application
      continue: true
    #   universal-bom-<bom>-<hw>-<ua>
    - test: always
      actions:
        - cl_process_params_to_profile universal-bom rack/bom universal/hardware universal/application
      continue: true
    - test: always
      actions:
        - cl_process_params_to_profile universal-hw rack/bom universal/hardware universal/application
      continue: true
    #   universal-bom-<hw>-<ua>
    - test: always
      actions:
        - cl_process_params_to_profile universal-bom universal/hardware universal/application
      continue: true
    - test: always
      actions:
        - cl_process_params_to_profile universal-hw universal/hardware universal/application
      continue: true
    #   universal-bom-<hw>
    - test: always
      actions:
        - cl_process_params_to_profile universal-bom universal/hardware
      continue: true
    - test: always
      actions:
        - cl_process_params_to_profile universal-hw universal/hardware
      continue: true
    #   universal-bom-<ua>
    - test: always
      actions:
        - cl_process_params_to_profile universal-bom universal/application
      continue: true
    - test: always
      actions:
        - cl_process_params_to_profile universal-hw universal/application
      continue: true
WF
else
  cat <<WF >> $_W_YAML
  default: []
WF
fi

  _W_YAML="${OUT_DIR}/params/${_NAME}-functions.yaml"
  cat <<WF > $_W_YAML
---
Name: ${_pname}-functions
Description: "The functions for the $_NAME base classifier."
Documentation: |
  This is the list of functions to add for the $_NAME base classifier.
Meta:
  color: yellow
  icon: shuffle
  populate: "true"
Schema:
  type: array
  items:
    type: string
WF
if [[ "$_NAME" == "universal-discover-classification-base" ]] ; then
  cat <<WF >> $_W_YAML
  default:
    - convert_rack_build_to_universal_application.tmpl
WF
else
  cat <<WF >> $_W_YAML
  default: []
WF
fi
}

function build_validation_stage() {
  local _NAME=$1
  local _pname=${_NAME/-/\/}
  local _W_YAML="${OUT_DIR}/stages/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: $_NAME
Description: "validation stage $_NAME for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Params:
  validation/list-parameter: "${_pname}"
Tasks:
  - validation-start
  - validation-stop
WF

  _W_YAML="${OUT_DIR}/params/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: ${_pname}
Description: "validation param ${_pname} for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Schema:
  type: array
  items:
    type: string
  default: []
WF
}

function build_flexiflow_stage() {
  local _NAME=$1
  local _pname=${_NAME/-/\/}
  local _W_YAML="${OUT_DIR}/stages/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: $_NAME
Description: "flexiflow stage $_NAME for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Params:
  flexiflow/list-parameter: "${_pname}"
Tasks:
  - flexiflow-start
  - flexiflow-stop
WF

  _W_YAML="${OUT_DIR}/params/$_NAME.yaml"
  cat <<WF > $_W_YAML
---
Name: ${_pname}
Description: "flexiflow param ${_pname} for universal workflow"
Documentation: ""
Meta:
  color: yellow
  icon: shuffle
  title: RackN Content
Schema:
  type: array
  items:
    type: string
  default: []
WF
}

# Build our content meta info files
function build_meta() {
  echo "Building meta data files for content bundle ... "
  printf "RackN, Inc." > ${OUT_DIR}/._Author.meta
  printf "https://gitlab.com/rackn/universal/tree/v4/universal" > ${OUT_DIR}/._CodeSource.meta
  printf "blue" > ${OUT_DIR}/._Color.meta
  printf "archive" > ${OUT_DIR}/._Icon.meta
  printf "RackN" > ${OUT_DIR}/._Copyright.meta
  printf "Generated Content - $STAMP" > ${OUT_DIR}/._Description.meta
  printf "Generated Content - $STAMP" > ${OUT_DIR}/._DisplayName.meta
  printf "https://docs.rackn.io/stable/redirect/?ref=rs_content_universal" > ${OUT_DIR}/._DocUrl.meta
  printf "RackN" > ${OUT_DIR}/._License.meta
  printf "uv-generated-$STAMP" > ${OUT_DIR}/._Name.meta
  printf "1000" > ${OUT_DIR}/._Order.meta
  printf "sane-exit-codes" > ${OUT_DIR}/._RequiredFeatures.meta
  printf "RackN" > ${OUT_DIR}/._Source.meta
  printf "enterprise,rackn,universa" > ${OUT_DIR}/._Tags.meta
  printf "$STAMP" > ${OUT_DIR}/._Version.meta
}

# simple exit helper function for short conditional command lines
function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }

# set our trap and exit functions
trap cleanup EXIT
trap 'got_trap $LASTNO $LINENO $BASH_COMMAND' ERR SIGINT SIGTERM SIGQUIT

# process our command line flags, we also set the WORKFLOW_MAP in this function
process_options $*
if (( $dbg )); then
    echo ""
    echo "WORKFLOW_NAMES:"
    echo "----------"
    printf '%s\n' "${!WORKFLOW_NAMES[@]}"
    echo ""
fi
echo ""

STAMP=$(date +v%Y.%m.%d-%H%M%S)

[[ "$CONTENT" ]] && printf "$STAMP\nGenerated content for the following components.\n" > ${OUT_DIR}/._Documentation.meta || true
[[ "$CONTENT" ]] && build_meta

for var in "${COMPONENTS[@]}"
do
  echo "Building: $var"
  build_workflow "$var" "Universal Workflow for the $var"
done

cd $OUT_DIR
BUNDLE="$OUT_DIR/$CONTENT.yaml"
DRPCLI=$(which drpcli) || true
if [[ "$DO_BUNDLE" ]]
then
  if [[ -n "$DRPCLI" ]]
  then
    echo "Running 'drpcli' bundle operation ... "
    drpcli contents bundle $BUNDLE
    [[ ! -r "$BUNDLE" ]] && xiterr 1 "Unable to read bundle file '$BUNDLE'"
  else
    echo "No 'drpcli' binary found in PATH ('$PATH')"
    echo "Not running 'drpcli contents bundle...' operation."
  fi
fi

if [[ "$DO_UPLOAD" ]]
then
  if [[ -n "$DRPCLI" ]]
  then
    [[ ! -r "$BUNDLE" ]] && xiterr 1 "Unable to read bundle file '$BUNDLE'"
    echo "Running 'drpcli' upload operation ... "
    drpcli contents upload $BUNDLE
    echo ""
    echo "WARNING:  If you are doing iterative runs of this script, you must remove"
    echo "          previous content packs that were uploaded - each content pack gets"
    echo "          a unique name to allow multiple content pack creations."
    echo ""
    echo "          TO REMOVE:  drpcli contents destroy vmware-generated-$STAMP"
    echo ""
  else
    echo "No 'drpcli' binary found in PATH ('$PATH')"
    echo "Not running 'drpcli contents upload...' operation."
  fi
fi
