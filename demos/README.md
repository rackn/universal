Universal Demos Content
=======================


This directory contains various Demo content packs that utilize the Universal Workflow.

Generally speaking; you will need to include the ``base`` content, which provides helpers
for Universal workflow that the other Demo content packs rely on.

Usage basically requires:

  * Universal Content (in ``../content`` directory)
  * Demos Base (the ``base`` directory)
  * Demo specific content pack (a directory in this location)


Current Demo Contents
---------------------

base:  base content that is used by other demo content packs

kvm-0:  Shane's KVM host and VM environment for testing Universal Workflow

tmo-r3r6:  The T-Mobile / Dell joint demo using Dell's SPS labs environment ``r3r6``

demo-r3r6:  Generic ESXi 6.7.0u2 / vCF 3.8.1 only demo in the Dell SPS environment ``r3r6``

The ``misc-non-content`` directory just contains various content pieces that were generated
that might be useful.


The ``make-demo-profiles.sh`` was used to generate initlal Profiles for the Dell r3r6
environment for classification.
