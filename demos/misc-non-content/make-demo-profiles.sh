#!/usr/bin/env bash
# builds classify data for Demo environment

###
#   DATA input format file should contain
#
#   BMC/IDRAC_IP  LAST_BOOT_MACADDR HOSTNME
#   172.17.92.203 24:6e:96:6a:40:34 r3r6-u03
###

P="demos/profiles"
BASE="dell-sps-r3r6"
DATA="dell-sps-data"
CLASSIFY="${P}/${BASE}-classify.yaml"
BASE_PROFILE="${P}/${BASE}-base.yaml"
GW="172.17.92.254"
NM="255.255.255.0"
APP="esxi670u2"

echo "Writing base profile '$BASE_PROFILE'"
> $BASE_PROFILE
cat << END_BASE >> $BASE_PROFILE
---
Name: ${BASE}-base
Description: "Base params profile for '${BASE}' environment."
Documentation: |
  Sets common base of Param settings for the '${BASE}' environment.  Intended to
  be used as an included Profile in the Machines specific Profile.

  Machine specific settings (hostname, ipaddr) should existing the Machine
  specific profile.

Meta:
  color: pink
  icon: world
  title: RackN Content
Params:
  universal/application: $APP
  dns-search-domains:
  - rackn.io
  - rackn.com
  - rebar.digital
  dns-servers:
  - 1.1.1.1
  - 1.0.0.1
  ntp-servers:
  - 0.us.pool.ntp.org
  - 1.us.pool.ntp.org
  - 2.us.pool.ntp.org
  vmware/esxi-version: select-vendor
  esxi/network-firstboot-type: manual
  esxi/network-firstboot-gateway: $GW
  esxi/network-firstboot-netmask: $NM
  esxi/shell-local: true
  esxi/shell-remote: true
  esxi/skip-tools: true
  esxi/vmnic-device: vmnic2
  ipmi/mode: racadm
  ipmi/username: root
  ipmi/password: calvin
  flash-list: []
  raid-clear-config: true
  raid-target-config:
    - DiskCount: max
      Encrypt: false
      RaidLevel: raid10

END_BASE

echo "Writing header for $CLASSIFY"
> $CLASSIFY
cat << EOF >> $CLASSIFY
---
Name: "${BASE}-classify"
Description: "Classification tests for '${BASE}' environment."
Documentation: |
  This sets the profiles for the "${BASE}" test environment for
  Universal Workflow.  It should be added to Global Profile for
  the classify engine to access and match against.

Meta:
  color: "pink"
  icon: "balance scale"
  title: "RackN"
Secure: false
Params:
  classify/classification-data:
EOF

IFS="
"
for D in $(cat ./$DATA)
do
  B=$(echo $D | awk ' { print $1 }')
  M=$(echo $D | awk ' { print $2 }')
  N=$(echo $D | awk ' { print $3 }')
  T=$(echo $B | cut -d"." -f4)
  (( T++ ))
  I="172.17.92.$T"

  echo "Writing classify actions for $N"
  echo "    - test: \"has_mac ${M}\"" >> $CLASSIFY
  echo "      actions:" >> $CLASSIFY
  echo "        - \"add_profile ${BASE}-${N}\"" >> $CLASSIFY

  echo "Writing classify custom profile for $N"
  PROF="${P}/${BASE}-${N}.yaml"
  > $PROF
  cat << EOP >> $PROF
---
Name: ${BASE}-${N}
Description: "Machine specific profile for '${N}'."
Documentation: "Requires ${BASE}-base profile included."
Meta:
  color: pink
  icon: world
  title: RackN Content
Profiles:
  - ${BASE}-base
Params:
  esxi/network-firstboot-hostname: ${N}
  esxi/network-firstboot-ipaddr: ${I}

EOP

done

